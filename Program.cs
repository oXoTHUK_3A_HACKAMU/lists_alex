﻿using System;
using System.Collections.Generic;

namespace Lists
{
    class Program
    {
        static void Program1(string[] args)
        {
            var rand = new Random();
            
            List<int> parts = new List<int>(10);
            for (int i = 0; i < 10; i++)
            {
                parts.Add(rand.Next(0, 10));
            }
            parts.RemoveAt(3);
            parts.RemoveAt(5);
            parts.Add(12);
            parts.ForEach(Console.WriteLine);
        }
    }
}
